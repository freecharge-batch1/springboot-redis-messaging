package com.tm.redis.message.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.redis.message.dto.EmployeeDto;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import java.util.ArrayList;

public class RedisMessageSubscriber implements MessageListener {

    @Override
    public void onMessage(Message message, byte[] bytes) {
        //messageList.add(message.toString());
        System.out.println("Message received : "+message.toString());
        EmployeeDto employeeDto = null;
        try {
            employeeDto = new ObjectMapper().readValue(message.toString(), EmployeeDto.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        System.out.println("EmployeeDto Object Received:" + employeeDto);
    }
}
