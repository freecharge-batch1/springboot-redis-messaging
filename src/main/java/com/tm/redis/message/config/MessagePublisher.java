package com.tm.redis.message.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tm.redis.message.dto.EmployeeDto;

public interface MessagePublisher  {

    void publish(EmployeeDto employeeDto) throws JsonProcessingException;
}
