package com.tm.redis.message.service;

import com.tm.redis.message.config.RedisMessagePublisher;
import com.tm.redis.message.dto.EmployeeDto;
import com.tm.redis.message.entity.Employee;
import com.tm.redis.message.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    RedisMessagePublisher messagePublisher;

    @Autowired
    private EmployeeRepository repository;

    @Override
    public Employee save(Employee employee) {
        Employee savedEmployee = null;
        try {
             savedEmployee = repository.save(employee);
            EmployeeDto employeeDto = EmployeeDto.builder().id(employee.getId())
                    .name(employee.getName())
                    .build();
            messagePublisher.publish(employeeDto);
        }
        catch(Exception e){
            e.printStackTrace();
            }


        return savedEmployee;
    }

    @Override
    public List<Employee> findAll() {
        return repository.findAll();
    }

    @Override
    @Cacheable(key = "#id", value = "Employee", unless = "#result.salary > 100000")
    public Employee findById(int id) {
        return repository.findById(id);
    }

    @Override
    @CachePut(key = "#id", value = "Employee")
    public Employee update(Employee employee) {
        return repository.update(employee);
    }

    @Override
    @CacheEvict(key = "#id", value = "Employee")
    public boolean delete(int id) {
        return repository.delete(id);
    }

}